<?php

class Format{
	
	public function amountValidate($data) {

		if (!preg_match('/^[0-9]+$/', $data)) {
	        echo "<p style='color:red'>Amount must be numbers.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }

	}


	public function buyerValidate($data) {

		if (!preg_match('/^[a-zA-Z0-9 ]+$/', $data)) {
	        echo "<p style='color:red'>Buyer must be text, spaces and numbers, not more than 20 characters.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }
	}


	public function receiptIdValidate($data) {

		if (!preg_match('/^[a-zA-Z]+$/i', $data)) {
	        echo "<p style='color:red'>Receipt ID must be text.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }
	}


	public function itemsValidate($data) {

		if (!preg_match('/^[a-zA-Z,]+$/i', $data)) {
	        echo "<p style='color:red'>Items must be text.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }
	}


	public function emailValidate($data) {
		
		if (!preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $data)) {
	        echo "<p style='color:red'>Enter a valid email address.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }
	}


	public function cityValidate($data) {

		if (!preg_match('/^[a-zA-Z ]+$/i', $data)) {
	        echo "<p style='color:red'>City must be text and spaces.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }
	}


	public function phoneValidate($data) {

		if (!preg_match('/^[0-9]+$/', $data)) {
	        echo "<p style='color:red'>Phone must be numbers.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }

	}

	public function entryByValidate($data) {

		if (!preg_match('/^[0-9]+$/', $data)) {
	        echo "<p style='color:red'>Entry by must be numbers.<p>";	        
	        return false;
	    } else {
	        return $data;
	    }

	}

}