<?php

session_start();

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "xpeedstudio");


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $timestamp = isset($_SESSION['form_submitted_ts'])
        ? $_SESSION['form_submitted_ts'] 
        : null;
    if(is_null($timestamp) || (time() - $timestamp) > 1 * 60) {
        if($_POST) {
            $_SESSION['form_submitted_ts'] = time();
            // echo 'success !';
        }
    } else {
        // Form submitted in last 24 hours.  Perhaps send HTTP 403 header.
        die('Failure! (Form submissions are rate limited.)');
    }
}



Class Database {

    public $host = DB_HOST;
    public $user = DB_USER;
    public $pass = DB_PASS;
    public $dbname = DB_NAME;
    public $link;
    public $error;

    public function __construct() {
        $this->connectDB();
    }

    private function connectDB() {
        $this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
        if (!$this->link) {
            $this->error = "Connection fail" . $this->link->connect_error;
            return false;
        }
    }


    // Insert data
    public function insert($query) {
        $insert_row = $this->link->query($query) or die($this->link->error . __LINE__);
        if ($insert_row) {
            return $insert_row;
        } else {
            return false;
        }
    }

}
