
    function amountValidate() {
        var amount = document.getElementById("amount").value;        

        if(!amount.match(/^[0-9]+$/)) {
            producePrompt("Amount must be numbers.", "amountStatus", "red","block");
            return false;
        } else {
            producePrompt("Amount is OK.", "amountStatus", "green","block");
            return true;
        }
    }


    function buyerValidate() {
        var buyer = document.getElementById("buyer").value;

        if(!buyer.match(/^[a-zA-Z0-9 ]+$/)) {
            producePrompt("Buyer must be text, spaces and numbers, not more than 20 characters.", "buyerStatus", "red","block");
            return false;    
        } else {
            producePrompt("Buyer is OK.", "buyerStatus", "green","block");
            return true; 
        }
    }


    function receiptIdValidate() {
        var receipt_id = document.getElementById("receipt_id").value;

        if(!receipt_id.match(/^[a-zA-Z]+$/i)) {
            producePrompt("Receipt ID must be text.", "receiptIdStatus", "red","block");
            return false;       
        } else {
            producePrompt("Receipt ID is OK.", "receiptIdStatus", "green","block");
            return true; 
        }
    }


    function itemsValidate() {
        var items = document.getElementById("items").value;

        if(!items.match(/^[a-zA-Z,]+$/i)) {
            producePrompt("Items must be text.", "itemsStatus", "red","block");
            return false;
        } else {
            producePrompt("Items is OK.", "itemsStatus", "green","block");
            return true;    
        }
    }


    function emailValidate() {
        var email = document.getElementById("buyer_email").value;

        if(!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            producePrompt("Enter a valid email address.", "emailStatus", "red","block");
            return false;  
        } else {
            producePrompt("Email is valid.", "emailStatus", "green","block");
            return true;  
        }
    }


    function cityValidate() {
        var city = document.getElementById("city").value;

        if(!city.match(/^[a-zA-Z ]+$/i)) {
            producePrompt("City must be text and spaces.", "cityStatus", "red","block");
            return false;
        } else {
            producePrompt("City is OK.", "cityStatus", "green","block");
            return true;
        }
    }

    function phoneValidate() {
        var phone = document.getElementById("phone").value;

        if(!phone.match(/^[0-9]+$/)) {
            producePrompt("Phone must be numbers.", "phoneStatus", "red","block");
            return false;
        } else {
            producePrompt("Phone is OK.", "phoneStatus", "green","block");
            return true;
        }
    }


    function entryByValidate() {
        var entry_by = document.getElementById("entry_by").value;

        if(!entry_by.match(/^[0-9]+$/)) {
            producePrompt("Entry by must be numbers.", "entryByStatus", "red","block");
            return false;
        } else {
            producePrompt("Entry by is OK.", "entryByStatus", "green","block");
            return true;
        }
    }


   function producePrompt(message, promptLocation, color, display) {
        document.getElementById(promptLocation).innerHTML = message;

        document.getElementById(promptLocation)
        .setAttribute("style", "color:"+color+';'+"display:"+display+';'+"margin-top:"+"-25px;"+"margin-bottom:"+"10px;");
    }