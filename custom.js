$(document).ready(function(){

    //Ajax Form Submit
    $("#ajaxStatus").hide();
    $("#submit").on('click', function () {
        var amount      = $("#amount").val();
        var buyer       = $("#buyer").val();
        var receipt_id  = $("#receipt_id").val();
        var items       = $("#items").val();
        var buyer_email = $("#buyer_email").val();
        var buyer_ip    = $("#buyer_ip").val();
        var note        = $("#note").val();
        var city        = $("#city").val();
        var phone       = $("#phone").val();
        var entry_by    = $("#entry_by").val();        
        $.ajax({
            url: "check.php",
            method: "POST",
            data: {amount:amount,buyer:buyer,receipt_id:receipt_id,items:items,buyer_email:buyer_email,buyer_ip:buyer_ip,note:note,city:city,phone:phone,entry_by:entry_by},
            datatype: "text",
            success: function (data) {
                $("#ajaxStatus").show();
                $("#ajaxStatus").html(data);
                $("#amount,#buyer,#receipt_id,#items,#buyer_email,#buyer_ip,#note,#city,#phone,#entry_by").val('');
            }
        });
        return false;
    });

});