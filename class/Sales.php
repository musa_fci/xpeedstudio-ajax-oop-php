<?php

include '././config/Database.php';
include '././config/Format.php';


class Sales{

	private $db;
    private $fm;
    public $ip;

    public function __construct() {
        $this->db = new Database();
        $this->fm = new Format();
    }



    public function addSaleInfo($data) {

    	$amount 		= $this->fm->amountValidate($data['amount']);
    	$buyer 			= $this->fm->buyerValidate($data['buyer']);
    	$receipt_id 	= $this->fm->receiptIdValidate($data['receipt_id']);
    	$items 			= $this->fm->itemsValidate($data['items']);
    	$buyer_email 	= $this->fm->emailValidate($data['buyer_email']);
    	$city 			= $this->fm->cityValidate($data['city']);
    	$phone 			= $this->fm->phoneValidate($data['phone']);
    	$entry_by 		= $this->fm->entryByValidate($data['entry_by']);


    	if($amount && $buyer && $receipt_id && $items && $buyer_email && $city && $phone && $entry_by) {

	        $amount 		= mysqli_real_escape_string($this->db->link, $amount);
	        $buyer 			= mysqli_real_escape_string($this->db->link, $buyer);
	        $receipt_id 	= mysqli_real_escape_string($this->db->link, $receipt_id);
	        $items 			= mysqli_real_escape_string($this->db->link, $items);
	        $buyer_email 	= mysqli_real_escape_string($this->db->link, $buyer_email);
	        $buyer_ip 		= mysqli_real_escape_string($this->db->link, $data['buyer_ip']);
	        $note 			= mysqli_real_escape_string($this->db->link, $data['note']);
	        $city 			= mysqli_real_escape_string($this->db->link, $city);
	        $phone 			= mysqli_real_escape_string($this->db->link, $phone);
	        $hash_key 		= sha1($receipt_id);        
	        $entry_by 		= mysqli_real_escape_string($this->db->link, $entry_by);

	        $query = "INSERT INTO sales_info (amount,buyer,receipt_id,items,buyer_email,buyer_ip,note,city,phone,hash_key,entry_at,entry_by)VALUES
	        ('$amount','$buyer','$receipt_id','$items','$buyer_email','$buyer_ip','$note','$city','$phone','$hash_key',NOW(),'$entry_by')";
	        $result = $this->db->insert($query);

	        if($result){
	        	echo "<span style='green'>Sale info save successfully !</span>";        	
	        	exit();
	        } else{
	        	echo "<span style='red'>Fail to save sale info !</span>";
	        	exit();
	        }
    	} else {
    		return false;
    	}

    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            echo $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            echo $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            echo $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            echo $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            echo $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            echo $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            echo $ipaddress = 'UNKNOWN';        
    }     

}