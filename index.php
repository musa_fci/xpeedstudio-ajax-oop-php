
<?php
    include './class/Sales.php';

    $sale  = new Sales();
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Sales Info</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
    <!-- Custom JS-->
    <script src="js/validation.js"></script>
    
</head>

<body>


    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading"></div>
                <div class="card-body">


                    <h2 class="title">Sales Info</h2>
                    
                    <div id="ajaxStatus"></div>

                    <form action="" method="POST" id="saleForm">
                        
                        <label>Amount</label>                            
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="amountValidate()" type="text" name="amount" id="amount">
                        </div>
                        <p id="amountStatus" style="display: none;"></p>

                        <label>Buyer</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="buyerValidate()" type="text" name="buyer" id="buyer">
                        </div>
                        <p id="buyerStatus" style="display: none;"></p>

                        <label>Receipt Id</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="receiptIdValidate()" type="text" name="receipt_id" id="receipt_id">
                        </div>
                        <p id="receiptIdStatus" style="display: none;"></p>


                        <label>Items (separate by comma)</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="itemsValidate()" type="text" name="items" id="items">
                        </div>
                        <p id="itemsStatus" style="display: none;"></p>




                        <label>Buyer Email</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="emailValidate()" type="text" name="buyer_email" id="buyer_email">
                        </div>
                        <p id="emailStatus" style="display: none;"></p>


                        <div class="">
                            <input class="" type="hidden" value="<?php $sale->get_client_ip(); ?>" placeholder="Buyer Ip" name="buyer_ip" id="buyer_ip">
                        </div>

                        <label>Note</label>
                        <div class="input-group">
                            <textarea class="input--style-1" cols="68" rows="3" name="note" id="note"></textarea>
                        </div>


                        <label>City</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="cityValidate()" type="text" name="city" id="city">
                        </div>
                        <p id="cityStatus" style="display: none;"></p>


                        <label>Phone</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="phoneValidate()" type="text" name="phone" id="phone">
                        </div>
                        <p id="phoneStatus" style="display: none;"></p>


                        <label>Entry By</label>
                        <div class="input-group">
                            <input class="input--style-1" onkeyup="entryByValidate()" type="text" name="entry_by" id="entry_by">
                        </div>
                        <p id="entryByStatus" style="display: none;"></p>



                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" onclick="return amountValidate(),buyerValidate(),receiptIdValidate(),itemsValidate(),emailValidate(),cityValidate(),phoneValidate(),entryByValidate()" type="submit" id="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="custom.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
